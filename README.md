 # Electronics

 Bienvenue sur la page GitLab electronique du projet AGV2.

 Vous y trouverez les differents fichiers des cartes electroniques d'alimentation/puissance du projet.

La V1 de la carte "Carte_alimentation_AGV" a ete concue pour fournir la protection des moteurs et des composants electoniques. Elle permet de limiter le courant envoyé aux moteurs, de proteger de la surtension et du branchement inverse de la batterie.

Malheuresement cette carte n'a pas eu le temps d'etre fabriquee. 

La V2 de la carte permet simplement de brancher des batteries en serie ou en paralelle (au choix). Elle ne fourni aucune protection. Le but de cette carte est juste d'avoir un interface entre les batteries et les moteurs rapide a fabriquer pour obtenir le prototype de l'AGV a temps.

Tout est expliqué plus en détails dans les différents rapports et réunion techniques à partir du S8

 Pour toute question : ihsane.kehli@insa-strasbourg.fr

